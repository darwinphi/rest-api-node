const express 	= require('express')
const router 		=	express.Router()
const mongoose 	= require('mongoose')
const bcrypt 		= require('bcrypt')
const jwt				= require('jsonwebtoken')
const checkAuth = require('../middleware/check-auth')

const User = require('../models/user')

const UserController = require('../controllers/user')

router.get('/', (req, res, next) => {
	User.find()
		.select('email _id')
		.exec()
		.then(users => {
			res.status(200).json({
				count: users.length,
				users: users.map(user => {
					return {
						_id: user._id,
						email: user.email
					}
				})
			})
		})
		.catch(err => {
			res.status(400).json({
				error: err
			})
		})
})

router.post('/signup', UserController.user_signup)

router.post('/login', UserController.user_login)

router.delete('/:userId', checkAuth, UserController.user_delete)

module.exports = router